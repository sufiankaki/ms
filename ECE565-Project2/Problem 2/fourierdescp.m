function z = fourierdescp(s)

[np, nc] = size(s);
x = 0:(np - 1);
m = ((-1) .^ x)';

s(:, 1) = m .* s(:, 1);
s(:, 2) = m .* s(:, 2);

s = s(:, 1) + i*s(:, 2);


z = fft(s);