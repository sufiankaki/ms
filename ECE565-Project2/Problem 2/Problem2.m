%Displaying the outer boundary
f=imread('Figure2.tif');
f=im2double(f);
[bound, L,N] = bwboundaries(f);
bound1 = bound{1};
mat_size = size(f);
boundary = zeros(mat_size);
for i = 1:length(bound1)
    boundary(bound1(i,1), bound1(i,2)) = 1;
end
figure;
imshow(boundary);
title('Original Image');

%Computing Fourier descriptors
fr = fourierdescp(bound1);


%Computing Inverse Fourier descriptors
ifr50 = round(abs(ifourierdescp(fr,length(fr)./2)));
ifr1 = round(abs(ifourierdescp(fr,length(fr)./100)));


boundary50 = zeros(mat_size);
boundary1 = zeros(mat_size);

for n = 1:length(ifr50)
    x=ifr50(n,1);
    y=ifr50(n,2);
    if(x==0)
        x=1;
    end
    if(y==0)
        y=1;
    end
    boundary50(x, y) = 1;
end

for n = 1:length(ifr1)
    x=ifr1(n,1);
    y=ifr1(n,2);
    if(x==0)
        x=1;
    end
    if(y==0)
        y=1;
    end
    boundary1(x, y) = 1;
end


figure;
imshow(boundary50);
title('Reconstruction with 50% Fourier descriptors');

figure;
imshow(boundary1);
title('Reconstruction with 1% Fourier descriptors');
