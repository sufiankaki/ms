#include <time.h>
#include <systemc.h>
#include "router.h"
#include "pe.h"

int c = 0;
int current_clock = 0;
extern int op, stage, router_queue[9], total_ip[10000], total_op[10000];

SC_MODULE(top)
{
public:
        enum {N = 3};
        // 2D array
        router *routers[N][N];
        PE_base *pes[N*N];
        sc_signal<packet> router_to_pe[N][N], pe_to_router[N][N];
        sc_signal<packet> router_to_router_east[N-1][N], router_to_router_west[N-1][N], router_to_router_north[N][N-1], router_to_router_south[N][N-1];
        sc_signal<packet> terminal_loop_north[N], terminal_loop_south[N];
        sc_signal<packet> terminal_loop_east[N], terminal_loop_west[N];
        sc_signal<bool> clock;
        SC_CTOR(top)
        {
                create_pes();
                create_network();
        }
protected:
        void create_pes()
        {
                pes[0] = new PE_inc("P1");
                pes[0]->clock(clock);
                pes[0]->set_xy(0, 0);
                pes[1] = new PE_inc("P2");
                pes[1]->clock(clock);
                pes[1]->set_xy(1, 0);
                pes[2] = new PE_inc("P3");
                pes[2]->clock(clock);
                pes[2]->set_xy(2, 0);

                pes[3] = new PE_inc("P4");
                pes[3]->clock(clock);
                pes[3]->set_xy(0, 1);
                pes[4] = new PE_IO("PI/PO");
                pes[4]->clock(clock);
                pes[4]->set_xy(1, 1);
                pes[5] = new PE_inc("P5");
                pes[5]->clock(clock);
                pes[5]->set_xy(2, 1);
                pes[6] = new PE_inc("P6");
                pes[6]->clock(clock);
                pes[6]->set_xy(0, 2);
                pes[7] = new PE_inc("P7");
                pes[7]->clock(clock);
                pes[7]->set_xy(1, 2);
                pes[8] = new PE_inc("P8");
                pes[8]->clock(clock);
                pes[8]->set_xy(2, 2);
        }

        void create_network()
        {
            for (int j=0; j < N; ++j) // For each row
            {
                for (int i = 0; i < N; ++i) // For each column
                {
                        char name[100];
                        sprintf(name, "router%d%d", i, j);
                        // create router
                        routers[i][j] = new router(name);
                        routers[i][j]->set_xy(i,j);
                        routers[i][j]->clock(clock);

                        // connect router to north routers
                        if (j != 0)
                        {
                                routers[i][j]->port_out[router::NORTH](router_to_router_north[i][j-1]);
                                routers[i][j]->port_in[router::NORTH](router_to_router_south[i][j-1]);
                        }
                        else // or make a loop
                        {
                                routers[i][j]->port_in[router::NORTH](
                                terminal_loop_north[i]);
                                routers[i][j]->port_out[router::NORTH](
                                terminal_loop_north[i]);
                        }

                        // connect router to south routers
                        if (j != N-1)
                        {
                                routers[i][j]->port_out[router::SOUTH](router_to_router_south[i][j]);
                                routers[i][j]->port_in[router::SOUTH](router_to_router_north[i][j]);
                        }
                        else // or make a loop
                        {
                                routers[i][j]->port_in[router::SOUTH](
                                terminal_loop_south[i]);
                                routers[i][j]->port_out[router::SOUTH](
                                terminal_loop_south[i]);
                        }
                        // connect router to west routers
                        if (i != 0)
                        {
                                routers[i][j]->port_out[router::WEST](
                                        router_to_router_west[i-1][j]);
                                routers[i][j]->port_in[router::WEST](
                                        router_to_router_east[i-1][j]);
                        }
                        else // or make a loop
                        {
                                routers[i][j]->port_out[router::WEST](
                                        terminal_loop_west[j]);
                                routers[i][j]->port_in[router::WEST](
                                        terminal_loop_west[j]);
                        }
                        if (i != N-1) // connect router to east routers
                        {
                                routers[i][j]->port_out[router::EAST](
                                        router_to_router_east[i][j]);
                                routers[i][j]->port_in[router::EAST](
                                        router_to_router_west[i][j]);
                        }
                        else // or make a loop
                        {
                                routers[i][j]->port_out[router::EAST](
                                        terminal_loop_east[j]);
                                routers[i][j]->port_in[router::EAST](
                                        terminal_loop_east[j]);
                        }
                        // connect router to PE
                        routers[i][j]->port_out[router::PE](router_to_pe[i][j]);
                        routers[i][j]->port_in[router::PE](pe_to_router[i][j]);
                        pes[c]->data_in(router_to_pe[i][j]);
                        pes[c]->data_out(pe_to_router[i][j]);
                        c=c+1; // No of pes
                }
            }
        }
}; // top


int sc_main(int argc , char *argv[])
{
        srand(0);
        top top_module("top");
        int clkcount = 5000;

        double throughput;
        int latency[10000], avg_latency=0, total_latency = 0, min_latency = 1000;
        printf("cycle 0 ================================\n");
        sc_start(0, SC_NS);


        for(int i = 1; i < clkcount; i++){
                printf("\ncycle %2d ================================\n", i);
                top_module.clock.write(1);
                sc_start(10, SC_NS);
                top_module.clock.write(0);
                sc_start(10, SC_NS);
                current_clock = i;
        }

        printf("\n\nInput sets fired : %d \n", stage-1);
        printf("Output sets fired : %d \n", op-1);

        throughput = float(stage - 1)/(float)clkcount;
        printf(" Maximum Throughput : %f \n",throughput);

        for (int i = 0; i < op-1; i++)
        {
                latency[i] = total_op[i] - total_ip[i];
                total_latency = total_latency + latency[i];
                if (latency[i] != 0)
                        if (latency[i] < min_latency)
                               min_latency = latency[i];
        }

        avg_latency = total_latency/(op-1);
        printf("Average Latency : %d \n", avg_latency);
        printf("Minimum Latency : %d \n", min_latency);

        return 0;
}

