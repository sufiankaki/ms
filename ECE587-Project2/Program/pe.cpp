#include "pe.h"

int i=0,n=0, stage = 1;
int setfcount = 0, op = 1, opcount[1000] = {1};
packet packet_in1_ [10] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,0};
packet packet_in2_ [10] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,0};
int level1[1000]={1}, level2[1000]={1}, level3[1000]={1}, level4[1000]={1};
extern int current_clock;
int router_queue[9]={0};
int total_ip[10000],total_op[10000];

void PE_base::set_xy(int x, int y)
{
        assert((x_ == -1) && (y_ == -1)); // set once only
        assert((x != -1) && (y != -1)); // must use a legal location

        x_ = x;
        y_ = y;
}

void PE_base::read_input()
{
        packet_in_ = data_in.read();
}

void PE_base::write_output()
{
        if (out_queue_.empty())
        {
                data_out.write(packet());
        }
        else
        {
                data_out.write(out_queue_.front());
                out_queue_.pop_front();
        }
}

void PE_IO::execute()
{
        // decide if we are going to fire PI
        int r = rand()%100;
        if (r < 80)
                fire_PI();

        // fire PO if the incoming packet is valid
        if ((packet_in_.src_x != -1)
                && (packet_in_.src_y != -1))
                fire_PO();
}


void PE_IO::fire_PI()
{
        int P1_x = 1, P1_y = 0, input_re[8] = {1,2,3,4,5,6,7,8}, input_im[8] = {8,7,6,5,4,3,2,1};

        for (i = 0, n = 0; i < 8; i++, n++)
        {
                if(i == 0 || i == 4){
                        P1_x = 0; P1_y = 0;}
                else if(i == 2 || i == 6){
                        P1_x = 0; P1_y = 1;}
                else if(i == 1 || i == 5){
                        P1_x = 0; P1_y = 2;}
                else if(i == 3 || i == 7){
                        P1_x = 1; P1_y = 0;}

                packet p(x_, y_, P1_x, P1_y, n, stage, input_re[i], input_im[i]);

                router_queue[P1_x*3+y_] = router_queue[P1_x*3+y_] + 1;
                out_queue_.push_back(p);
        }

        printf("Sent a set of packets. Set: %d \n", stage);
        total_ip[stage] = current_clock;
        stage++;
}


void PE_IO::fire_PO()
{
        assert((packet_in_.src_x != -1)
                && (packet_in_.src_y != -1));

        printf("Receive x[%d] = %f + %fi at (%d,%d)\n", packet_in_.pkt, packet_in_.token_real, packet_in_.token_img, packet_in_.src_x, packet_in_.src_y);

        PO_fire_count++;

        if((PO_fire_count == 8) && (level1[op]>3) && (level2[op]>3) && (level3[op]>3) && (level4[op]>3))
        {
                PO_fire_count=0;
                total_op[op] = current_clock;
                op++;
                opcount[op] = 1;
                printf("Received Set %d \n",op-1);
        }
}

void PE_inc::execute()
{
        // fire the actor if the incoming packet is valid
        if ((packet_in_.src_x != -1)
                && (packet_in_.src_y != -1))
                fire();
}

void PE_inc::fire()
{
        assert((packet_in_.src_x != -1)
                && (packet_in_.src_y != -1));

        int newpkt1=0, newpkt2=0;
        int sel=0;
        double InpR1=0, InpI1=0, InpR2=0, InpI2=0;
        double W1R = 1/1.414, W1I = -(1/1.414), W2R = 0, W2I = -1, W3R = -(1/1.414), W3I = -(1/1.414);

        if((packet_in_.stg)==op){
        if(opcount[op]==0){
        if((packet_in1_[x_*3+y_].src_x == -1) && (packet_in1_[x_*3+y_].src_y == -1) && (packet_in2_[x_*3+y_].src_x == -1) && (packet_in2_[x_*3+y_].src_y == -1 ) && (packet_in1_[x_*3+y_].stg == packet_in2_[x_*3+y_].stg)){
                packet_in1_[x_*3+y_] = packet_in_;
                setfcount = setfcount +1;
        }
        else if((packet_in2_[x_*3+y_].src_x == -1) && (packet_in2_[x_*3+y_].src_y == -1) ){
                packet_in2_[x_*3+y_] = packet_in_;
                setfcount = setfcount +1;
        }
        }
        else if (((packet_in_.stg > packet_in1_[x_*3+y_].stg) || (packet_in_.stg > packet_in2_[x_*3+y_].stg)) && (opcount[op] == 1)){
        if(packet_in_.stg > packet_in1_[x_*3+y_].stg){
                packet_in1_[x_*3+y_] = packet_in_;
                setfcount = setfcount +1;
        }
        else if (packet_in_.stg > packet_in2_[x_*3+y_].stg){
                packet_in2_[x_*3+y_] = packet_in_;
                setfcount = setfcount +1;
        }
        }
        }
        else if (packet_in_.stg > op){
                out_queue_.push_back(packet_in_);
        }
        if(setfcount == 8 ){
                setfcount = 0;
                opcount[op] = 0;
        }

        if((packet_in1_[x_*3+y_].src_x != -1) && (packet_in1_[x_*3+y_].src_y != -1) &&(packet_in2_[x_*3+y_].src_x != -1) && (packet_in2_[x_*3+y_].src_y != -1) && (packet_in1_[x_*3+y_].stg == packet_in_.stg) &&(packet_in_.stg ==
packet_in2_[x_*3+y_].stg)){
        if((packet_in1_[x_*3+y_].pkt) > (packet_in2_[x_*3+y_].pkt)){
                packet_in1_[9] = packet_in1_[x_*3+y_];
                packet_in2_[9] = packet_in2_[x_*3+y_];
                packet_in1_[x_*3+y_] = packet_in2_[9];
                packet_in2_[x_*3+y_] = packet_in1_[9];
                packet_in1_[9] = -1;
                packet_in2_[9] = -1;
        }

        switch(x_*3+y_){
        case 0:
                level1[packet_in_.stg]++;
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=0;
                        packet p(x_, y_, 2, 0, newpkt1, packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 2, 0);
                        out_queue_.push_back(p);
                        router_queue[6] = router_queue[6] + 1;
                        sel=1;
               }
               if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;

                        newpkt2=1;
                        packet p(x_, y_, 2, 1, newpkt2, packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 2, 1);
                        out_queue_.push_back(p);
                        router_queue[7] = router_queue[7] + 1;
                        sel=0;
                }
                level1[packet_in_.stg]++;
                break;
        case 1:
                level2[packet_in_.stg]++;
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=2;
                        packet p(x_, y_, 2, 0, newpkt1, packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 2, 0);
                        out_queue_.push_back(p);
                        router_queue[6] = router_queue[6] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;
                        newpkt2=3;
                        packet p(x_, y_, 2, 1, newpkt2, packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 2, 1);
                        out_queue_.push_back(p);
                        router_queue[7] = router_queue[7] + 1;
                        sel=0;
                }
                level2[packet_in_.stg]++;
                break;
        case 2:
                level3[packet_in_.stg]++;
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=4;
                        packet p(x_, y_, 1, 2, newpkt1, packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 1, 2);
                        out_queue_.push_back(p);
                        router_queue[5] = router_queue[5] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;
                        newpkt2=5;
                        packet p(x_, y_, 2, 2, newpkt2, packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", x_, y_, InpR2, InpI2, 2, 2);
                        out_queue_.push_back(p);
                        router_queue[8] = router_queue[8] + 1;
                        sel=0;
                }
                level3[packet_in_.stg]++;
                break;
        case 3:
                level4[packet_in_.stg]++;
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=6;
                        packet p(x_, y_, 1, 2, newpkt1, packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 1, 2);
                        out_queue_.push_back(p);
                        router_queue[5] = router_queue[5] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;
                        newpkt2=7;
                        packet p(x_, y_, 2, 2, newpkt2,packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 2, 2);
                        out_queue_.push_back(p);
                        router_queue[8] = router_queue[8] + 1;
                        sel=0;
                }
                level4[packet_in_.stg]++;
                break;
        case 5:
                if(level3[packet_in_.stg]==2){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=4;
                        packet p(x_, y_, 2, 0, newpkt1,packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 2, 0);
                        out_queue_.push_back(p);
                        router_queue[6] = router_queue[6] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;
                        newpkt2=5;
                        packet p(x_, y_, 1, 2, newpkt2,packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 1, 2);
                        out_queue_.push_back(p);
                        router_queue[5] = router_queue[5] + 1;
                        sel=0;
                }
                level3[packet_in_.stg]++;
                }
                else if(level3[packet_in_.stg]==3){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + ((packet_in2_[x_*3+y_].token_real*W2R)-(packet_in2_[x_*3+y_].token_img*W2I));
                        InpI1 = packet_in1_[x_*3+y_].token_img + ((packet_in2_[x_*3+y_].token_real*W2I)+(packet_in2_[x_*3+y_].token_img*W2R));
                        newpkt1=2;
                        packet p(x_, y_, 1, 1, newpkt1,packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=1;
                }

                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - ((packet_in2_[x_*3+y_].token_real*W2R)-(packet_in2_[x_*3+y_].token_img*W2I));
                        InpI2 = packet_in1_[x_*3+y_].token_img - ((packet_in2_[x_*3+y_].token_real*W2I)+(packet_in2_[x_*3+y_].token_img*W2R));
                        newpkt2=6;
                        packet p(x_, y_, 1, 1, newpkt2, packet_in_.stg,InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=0;
                }
                level3[packet_in_.stg]++;
                }
                break;
        case 6:
                if(level1[packet_in_.stg]==2){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=0;
                        packet p(x_, y_, 2, 0, newpkt1, packet_in_.stg,InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 2, 0);
                        out_queue_.push_back(p);
                        router_queue[6] = router_queue[6] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;
                        newpkt2=1;
                        packet p(x_, y_, 1, 2, newpkt2, packet_in_.stg,InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 1, 2);
                        out_queue_.push_back(p);
                        router_queue[5] = router_queue[5] + 1;
                        sel=0;
                }
                level1[packet_in_.stg]++;
                }
                else if(level1[packet_in_.stg]==3){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + packet_in2_[x_*3+y_].token_real;
                        InpI1 = packet_in1_[x_*3+y_].token_img + packet_in2_[x_*3+y_].token_img;
                        newpkt1=0;
                        packet p(x_, y_, 1, 1, newpkt1,packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - packet_in2_[x_*3+y_].token_real;
                        InpI2 = packet_in1_[x_*3+y_].token_img - packet_in2_[x_*3+y_].token_img;
                        newpkt2=4;
                        packet p(x_, y_, 1, 1, newpkt2,packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=0;
                }


                level1[packet_in_.stg]++;
                }
                break;
        case 7:
                if(level2[packet_in_.stg]==2){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + ((packet_in2_[x_*3+y_].token_real*W2R)-(packet_in2_[x_*3+y_].token_img*W2I));
                        InpI1 = packet_in1_[x_*3+y_].token_img + ((packet_in2_[x_*3+y_].token_real*W2I)+(packet_in2_[x_*3+y_].token_img*W2R));
                        newpkt1=2;
                        packet p(x_, y_, 2, 1, newpkt1,packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 2, 1);
                        out_queue_.push_back(p);
                        router_queue[7] = router_queue[7] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - ((packet_in2_[x_*3+y_].token_real*W2R)-(packet_in2_[x_*3+y_].token_img*W2I));
                        InpI2 = packet_in1_[x_*3+y_].token_img - ((packet_in2_[x_*3+y_].token_real*W2I)+(packet_in2_[x_*3+y_].token_img*W2R));
                        newpkt2=3;
                        packet p(x_, y_, 2, 2, newpkt2, packet_in_.stg,InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 2, 2);
                        out_queue_.push_back(p);
                        router_queue[8] = router_queue[8] + 1;
                        sel=0;
                }
                level2[packet_in_.stg]++;
                }
                else if(level2[packet_in_.stg]==3){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + ((packet_in2_[x_*3+y_].token_real*W1R)-(packet_in2_[x_*3+y_].token_img*W1I));
                        InpI1 = packet_in1_[x_*3+y_].token_img + ((packet_in2_[x_*3+y_].token_real*W1I)+(packet_in2_[x_*3+y_].token_img*W1R));
                        newpkt1=1;
                        packet p(x_, y_, 1, 1, newpkt1,packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - ((packet_in2_[x_*3+y_].token_real*W1R)-(packet_in2_[x_*3+y_].token_img*W1I));
                        InpI2 = packet_in1_[x_*3+y_].token_img - ((packet_in2_[x_*3+y_].token_real*W1I)+(packet_in2_[x_*3+y_].token_img*W1R));
                        newpkt2=5;
                        packet p(x_, y_, 1, 1, newpkt2,packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=0;
                        }
                level2[packet_in_.stg]++;
                }
                break;
        case 8:
                if(level4[packet_in_.stg]==2){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + ((packet_in2_[x_*3+y_].token_real*W2R)-(packet_in2_[x_*3+y_].token_img*W2I));
                        InpI1 = packet_in1_[x_*3+y_].token_img + ((packet_in2_[x_*3+y_].token_real*W2I)+(packet_in2_[x_*3+y_].token_img*W2R));
                        newpkt1=6;
                        packet p(x_, y_, 2, 1, newpkt1, packet_in_.stg,InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 2, 1);
                        out_queue_.push_back(p);
                        router_queue[7] = router_queue[7] + 1;
                        sel =1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - ((packet_in2_[x_*3+y_].token_real*W2R)-(packet_in2_[x_*3+y_].token_img*W2I));
                        InpI2 = packet_in1_[x_*3+y_].token_img - ((packet_in2_[x_*3+y_].token_real*W2I)+(packet_in2_[x_*3+y_].token_img*W2R));
                        newpkt2=7;
                        packet p(x_, y_, 2, 2, newpkt2, packet_in_.stg,InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 2, 2);
                        out_queue_.push_back(p);
                        router_queue[8] = router_queue[8] + 1;
                        sel=0;
                }
                level4[packet_in_.stg]++;
                }
                else if(level4[packet_in_.stg]==3){
                if(sel==0){
                        InpR1 = packet_in1_[x_*3+y_].token_real + ((packet_in2_[x_*3+y_].token_real*W3R)-(packet_in2_[x_*3+y_].token_img*W3I));
                        InpI1 = packet_in1_[x_*3+y_].token_img + ((packet_in2_[x_*3+y_].token_real*W3I)+(packet_in2_[x_*3+y_].token_img*W3R));
                        newpkt1=3;
                        packet p(x_, y_, 1, 1, newpkt1,packet_in_.stg, InpR1, InpI1);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR1, InpI1, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=1;
                }
                if(sel==1){
                        InpR2 = packet_in1_[x_*3+y_].token_real - ((packet_in2_[x_*3+y_].token_real*W3R)-(packet_in2_[x_*3+y_].token_img*W3I));
                        InpI2 = packet_in1_[x_*3+y_].token_img - ((packet_in2_[x_*3+y_].token_real*W3I)+(packet_in2_[x_*3+y_].token_img*W3R));
                        newpkt2=7;
                        packet p(x_, y_, 1, 1, newpkt2,packet_in_.stg, InpR2, InpI2);
                        printf("Sent %f + %fi to (%d,%d)\n", InpR2, InpI2, 1, 1);
                        out_queue_.push_back(p);
                        router_queue[4] = router_queue[4] + 1;
                        sel=0;
                }
                level4[packet_in_.stg]++;
                }
                break;
        default: break;
        }

        packet_in1_[x_*3+y_].src_x=-1;
        packet_in1_[x_*3+y_].src_y=-1;
        packet_in2_[x_*3+y_].src_x=-1;
        packet_in2_[x_*3+y_].src_y=-1;
        router_queue[x_*3+y_] = router_queue[x_*3+y_] - 2;
        }
}

