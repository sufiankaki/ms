library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.STD_LOGIC_misc.all;
use ieee.STD_LOGIC_unsigned.all;

entity processor is
	port( clk : in STD_LOGIC);
 end entity;

architecture processorbhv of processor is 

component ALU
	port (  A,B : in STD_LOGIC_VECTOR(31 downto 0);		--Inputs to ALU
       		ALUOut : inout STD_LOGIC_VECTOR(31 downto 0); 	--Output from ALU
		ALUOp : in STD_LOGIC_VECTOR( 3 downto 0);	--ALU control line
       		zero : out STD_LOGIC ;                          --zero detect
       		ovf : out STD_LOGIC);                		--Overflow detect
end component;

component RegFile is
   	port(	clk,WriteEnable  : in STD_LOGIC;                -- Clock, Write Enable 
		Rs,Rt,Rd : in  STD_LOGIC_VECTOR(4 downto 0);    -- Register select
		RsOut,RtOut : out STD_LOGIC_VECTOR(31 downto 0);-- Data from Rs, Rt 
		WriteData : in STD_LOGIC_VECTOR(31 downto 0));   -- Data to RegFile: Rt/Rd
end component;

component DataMem
	port(	clk, DMRead, DMWrite, DCHit : in STD_LOGIC; 			-- Clock, Control lines
       		DAddr  : in STD_LOGIC_VECTOR  (31 downto 0);		-- Memory Address
 		WriteData  : in STD_LOGIC_VECTOR  (127 downto 0);	-- Data to be written
       		ReadData : out STD_LOGIC_VECTOR (127 downto 0));		-- Data read from ROM
end component;

component DCache is
	port(	clk, DCRead, DCWrite:	in STD_LOGIC;
		DCHit:	out STD_LOGIC;
		DAddr: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		MDR: 	out STD_LOGIC_VECTOR(31 downto 0);	-- Instruction
		MDR_In:	in STD_LOGIC_VECTOR(31 downto 0);
		MDR_BlkIn:	in STD_LOGIC_VECTOR(127 downto 0);
		MDR_BlkOut:	out STD_LOGIC_VECTOR(127 downto 0));
end component DCache;

component ContUnit 
	port(	OpCode,Funct : in STD_LOGIC_VECTOR(5 downto 0);	-- op code/function field
		RegDst, RegWrite: out STD_LOGIC;		-- Reg dest selection (Rt:0/Rd:1), Reg Write Enable
		ALUSrc : out STD_LOGIC;				-- ALU Source 2 (Reg File or Instruction)
		ALUOp : out STD_LOGIC_VECTOR(3 downto 0);	-- ALUOp control line
		MemWrite: out STD_LOGIC;			-- MemWrite control line
		MemRead : out STD_LOGIC;			-- MemRead control line
		MemToReg : out STD_LOGIC;			-- MemToReg control line
		Branch : out STD_LOGIC;				-- Branch control line
		Jump : out STD_LOGIC;
		BNE : out STD_LOGIC;
		jal : out STD_LOGIC);
end component;

component InstrMem
	port(	clk,ICHit:	in STD_LOGIC;
		MemBusy:out STD_LOGIC;
		PC: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		IRBlock: out STD_LOGIC_VECTOR(127 downto 0));	-- Instruction

end component;

component ICache is
	port(	clk, Mode:	in STD_LOGIC;
		ICHit:	out STD_LOGIC;
		PC: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		IR: 	out STD_LOGIC_VECTOR(31 downto 0);	-- Instruction
		IR_Inc:	in STD_LOGIC_VECTOR(127 downto 0));

end component ICache;

component DMA is
	port(	clk:	in STD_LOGIC;
		PC:	in STD_LOGIC_VECTOR(31 downto 0));
end component DMA;


signal RegDst, RegWrite, ALUSrc, MemWrite, MemRead, MemToReg, Branch, zero, ovf,MemBusy, ICHit, Jump, bne,jal,DCHit : STD_LOGIC := '0';
signal MemIR, DM2DC, DC2DM: STD_LOGIC_VECTOR(127 downto 0);
signal ALUIn1, ALUIn2, WriteData, ALUOut, DAddr : STD_LOGIC_VECTOR(31 downto 0); 
signal DCDataOut, NewPC : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
signal Rs,Rt,Rd : STD_LOGIC_VECTOR(4 downto 0) := (others => '0');  
signal OpCode, Funct : STD_LOGIC_VECTOR(5 downto 0) := (others => '0');  
signal ALUOp : STD_LOGIC_VECTOR( 3 downto 0) := (others => '0');
signal Imm32, RtOut: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
signal PC: STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
signal Mode:STD_LOGIC;
signal IR: STD_LOGIC_VECTOR(31 downto 0);


begin
	-- Control unit
	CT: ContUnit port map(OpCode,Funct,RegDst,RegWrite,ALUSrc,ALUOp,MemWrite,MemRead,MemToReg,Branch,Jump, bne, jal);

	-- Data Memory
	DM: DataMem port map(clk,MemRead,MemWrite, DCHit, DAddr,DC2DM,DM2DC);

	-- Data Memory
	DC: DCache port map(clk,MemRead,MemWrite, DCHit, DAddr,DCDataOut,RtOut, DM2DC, DC2DM);

	-- Register file
	RF: RegFile port map(clk,RegWrite,Rs,Rt,Rd,ALUIn1,RtOut,WriteData);

	-- Arithmetic logic unit
	AL: ALU port map(ALUIn1,ALUIn2,ALUOut,ALUOp,zero,ovf);
		
	-- Instruction Memory
	II: InstrMem port map(clk,ICHit,MemBusy,PC,MemIR);

	-- Instruction Cache
	IC: ICache port map(clk,Mode,ICHit,PC,IR, MemIR);

	-- DMA
	DMAEx: DMA port map(clk,PC);


	-- Get opcode for control unit
	OpCode <= IR(31 downto 26);

	-- Get function field
	Funct <= IR(5 downto 0);

	-- Set rs register
	Rs <= IR(25 downto 21);

	-- Set rt register
	Rt <= IR(20 downto 16);

	-- Sign extend the imm16 to imm32
	Imm32<= STD_LOGIC_VECTOR(resize(signed(IR(15 downto 0)), 32));

	-- MUX chose the REGISTER destination
	Rd <= IR(15 downto 11) when (RegDst = '1' and jal = '0') else "11111" when jal = '1' else IR(20 downto 16);

	-- MUX chose the MEMORY/ALU writeback
	WriteData <= DCDataOut when (MemToReg = '1' and jal = '0') else PC when jal = '1' else ALUOut;
	
	-- MUX chose data2 from REGFILE/IMM32
	ALUIn2 <= Imm32  when ALUSrc = '1' else RtOut;

	DAddr <= ALUOut after 5 ns;

	

	process(clk)
	begin
		-- updating the pc on the falling edge
		if falling_edge(clk) then	
			if ICHit = '1' and Mode = '0' then			
			if((Branch = '1' and zero = '1') or (Jump = '1') or (bne = '1' and zero = '0')) then
				-- take the branch
				 PC <= PC + to_integer(signed(Imm32)) + 4;
			else
				-- go to next instruction
				PC <= PC + 4;
			end if;
			end if;
			if ICHit = '1' then
				Mode <= '0';
			elsif ICHit = '0' then
				Mode <= '1';
			end if;
		end if;
			
	end process;

end processorbhv;
