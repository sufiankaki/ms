library IEEE;
use IEEE.std_logic_1164.all;  -- for the use of STD_LOGIC/_VECTOR
use IEEE.numeric_std.ALL;     -- for the use of to_integer function

entity ICache is
	port(	clk, Mode:	in STD_LOGIC;
		ICHit:	out STD_LOGIC;
		PC: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		IR: 	out STD_LOGIC_VECTOR(31 downto 0);	-- Instruction
		IR_Inc:	in STD_LOGIC_VECTOR(127 downto 0));
end entity ICache;

architecture ICacheBhv of ICache is
	type Memory is array(0 to 511) of std_logic_vector(7 downto 0);
	signal Instructions : Memory;
	type Tag is array(0 to 511) of std_logic_vector(1 downto 0);
	signal InsTag : Tag;--:= (others=> (others=>'0'));
	signal temp_out: std_logic_vector(31 downto 0);
	signal temp_in: std_logic_vector(127 downto 0);
	signal temp:STD_LOGIC;
begin
		temp_out(31 downto 24) <= Instructions(to_integer(unsigned(PC))+0);
		temp_out(23 downto 16) <= Instructions(to_integer(unsigned(PC))+1);
		temp_out(15 downto 08) <= Instructions(to_integer(unsigned(PC))+2);
		temp_out(07 downto 00) <= Instructions(to_integer(unsigned(PC))+3);

		temp_in <= IR_Inc;

	process(clk)
	begin 
	if  rising_edge(clk) then 
	if InsTag(to_integer(unsigned(PC(8 downto 0)))) = PC(10 downto 9) then
		ICHit <= '1';
	else
		ICHit <= '0';
	end if;
	if Mode = '0' then
		 IR <= temp_out;		
	elsif (Mode = '1' and temp_in(127 downto 124) /= "UUUU") then	
		for n in 0 to 15 loop
		Instructions(to_integer(unsigned(PC))+n) <= temp_in((127-8*n) downto (120-8*n));
		InsTag(to_integer(unsigned(PC(8 downto 0))+n)) <= PC(10 downto 9);
		end loop;
	end if;
	end if;
	end process;
end ICacheBhv; 