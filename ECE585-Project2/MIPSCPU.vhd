
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_unsigned.all;

entity mipscpu is 


end entity;

architecture mipscpubhv of mipscpu is 
	signal clk : STD_LOGIC := '0';

	component processor is
		port( clk : in STD_LOGIC);
	end component;
begin
	clk <= not clk after 25 ns;	-- Generates clock


p1: processor port map(clk);

end mipscpubhv; 
