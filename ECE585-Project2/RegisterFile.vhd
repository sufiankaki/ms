library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.ALL;   

entity RegFile is
	port(	clk,WriteEnable  : in STD_LOGIC;                -- Clock, Write Enable 
		Rs,Rt,Rd : in  STD_LOGIC_VECTOR(4 downto 0);    -- Register select
		RsOut,RtOut : out STD_LOGIC_VECTOR(31 downto 0);-- Data from Rs, Rt 
		WriteData : in STD_LOGIC_VECTOR(31 downto 0));   -- Data to RegFile: Rt/Rd
end entity;

architecture RegFilebhv of RegFile is
        
	 
         type RF_Typ is array(0 to 31) of STD_LOGIC_VECTOR(31 downto 0); 

	 signal RF : RF_Typ := (x"00000000",x"00000000",	--$zero, $at
				x"00000000",x"00000000",	--$v0, $v1
				x"00000000",x"00000000",	--$a0, $a1
				x"00000000",x"00000000",	--$a2, $a3 
				x"00000000",x"00000000",	--$t0, $t1
				x"00000000",x"1122AABB",	--$t2, $t3
				x"11111111",x"00002222",	--$t4, $t5
				x"00000000",x"01234567",	--$t6, $t7
				x"00000000",x"00000000",	--$s0, $s1
				x"FFFFFF9C",x"00000000",	--$s2, $s3
				x"1122AABB",x"11111111",	--$s4, $s5
				others=> (others=>'0'));	--others
         

begin
	RsOut <= RF(to_integer(unsigned(Rs)));               
	RtOut <= RF(to_integer(unsigned(Rt)));              	
		       

	process(clk)
        begin   
		if (rising_edge(clk) and WriteEnable = '1' and Rd /= "00000") then     

			RF(to_integer(unsigned(Rd)))<= WriteData;  
		end if;	
	end process;
end RegFilebhv;
