library IEEE;
use IEEE.std_logic_1164.all;  -- for the use of STD_LOGIC/_VECTOR
use IEEE.numeric_std.ALL;     -- for the use of to_integer function

entity InstrMem is
	port(	clk,ICHit:	in STD_LOGIC;
		MemBusy:out STD_LOGIC;		
		PC  : in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		IRBlock: out STD_LOGIC_VECTOR(127 downto 0));	-- Instruction

end entity;

architecture InstrMembhv of InstrMem is
	type Memory is array(0 to 2047) of std_logic_vector(7 downto 0);
	signal Instructions : Memory := 
		(x"8E",x"49",x"00",x"64",  -- 1 lw $t1, 100($s2)
		x"AE",x"4F",x"01",x"2C",   -- 2 sw $t7, 300($s2)
		x"02",x"8C",x"50",x"20",   -- 3 add $t2, $s4, $t4
		x"12",x"B1",x"01",x"2C",   -- 4 beq $s5, $s1, 300
		x"35",x"93",x"00",x"C8",   -- 5 xori $s3, $t4, 200
		x"08",x"00",x"01",x"2C",   -- 6 j 300
		x"15",x"74",x"01",x"2C",   -- 7 bne $t3,$s4,300	--Custom Set Instruction 1 (Student ID ending with 7)
		x"3C",x"0D",x"AB",x"CD",   -- 8 lui $t5, 0xabcd	--Custom Set Instruction 2 (Student ID ending with 7)
		x"0C",x"00",x"20",x"00",   -- 9 jal 0x2000	--Custom Set Instruction 3 (Student ID ending with 7)
		others => (others =>'X'));
	type ReadDelay is array (0 to 2) of std_logic_vector(7 downto 0);
	signal InsQueue: ReadDelay; --:= (others=> (others=>'X'));
	signal temp: std_logic_vector(127 downto 0);

	begin
	Transfer:process(clk)	
	begin 
	
		for i in 0 to 15 loop
      			 temp((127-8*i) downto (120-8*i)) <= Instructions(to_integer(unsigned(PC))+i);
--			wait for 50 ns;
		end loop;
--		wait for 200 ns;
	if ICHit = '0' then
 		if  falling_edge(clk) then 
			IRBlock <= temp after 800 ns;
		end if;
	end if;
	end process;

end InstrMembhv; 