library IEEE;
use IEEE.std_logic_1164.all;  -- for the use of STD_LOGIC/_VECTOR
use IEEE.numeric_std.ALL;     -- for the use of to_integer function

entity DMA is
	port(	clk:	in STD_LOGIC;
		PC: in STD_LOGIC_VECTOR(31 downto 0));
end entity DMA;

architecture DMABhv of DMA is

component InstrMem
	port(	clk,ICHit:	in STD_LOGIC;
		MemBusy:out STD_LOGIC;
		PC: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		IRBlock: 	out STD_LOGIC_VECTOR(127 downto 0));	-- Instruction
end component;

component ICache is
	port(	clk,Mode:	in STD_LOGIC;
		ICHit:	out STD_LOGIC;
		PC: 	in STD_LOGIC_VECTOR(31 downto 0);	-- Instruction Address
		IR: 	out STD_LOGIC_VECTOR(31 downto 0);	-- Instruction
		IR_Inc:	in STD_LOGIC_VECTOR(127 downto 0));
end component ICache;

signal InstrMemIR,temp: STD_LOGIC_VECTOR(127 downto 0);
signal ICacheIR: STD_LOGIC_VECTOR(31 downto 0);
signal MemBusy,ICHit,Mode: STD_LOGIC;

begin
DMAIM: InstrMem port map(clk, ICHit, MemBusy, PC, InstrMemIR);
DMAIC: ICache port map(clk,Mode,  ICHit, PC, ICacheIR, temp);


InsTransfer:process(clk)
	begin
	
	if ICHit = '0' then
		Mode <= '1';
		
			temp <= InstrMemIR;
		

	end if;
end process;
	
end DMABhv; 
