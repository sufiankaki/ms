library IEEE;
use IEEE.std_logic_1164.all;  -- for the use of STD_LOGIC/_VECTOR
use IEEE.numeric_std.all;     -- for the use of to_integer Functtion


entity ContUnit is
	port(	OpCode,Funct : in STD_LOGIC_VECTOR(5 downto 0);	-- OpCode code/Functtion field
		RegDst, RegWrite: out STD_LOGIC;		-- Reg dest selection (Rt:0/Rd:1), Reg Write Enable
		ALUSrc : out STD_LOGIC;				-- ALU Source 2 (Reg File or Instruction)
		ALUOp : out STD_LOGIC_VECTOR(3 downto 0);	-- ALUOpCode control line
		MemWrite: out STD_LOGIC;			-- MemWrite control line
		MemRead : out STD_LOGIC;			-- MemRead control line
		MemToReg : out STD_LOGIC;			-- MemToReg control line
		Branch : out STD_LOGIC;				-- Branch control line
		Jump : out STD_LOGIC;
		BNE : out STD_LOGIC;
		jal : out STD_LOGIC);
end entity;


architecture ContUnitbhv of ContUnit is
	signal c : STD_LOGIC_VECTOR(13 downto 0);
begin
      ALUOp <= c(7 downto 4); 

	c <= 	"00001100100110" when OpCode = "100011"  else 				--lw 
		"00000100101000" when OpCode = "101011"  else 				--sw
		"00011000100000" when OpCode = "000000"  and Funct = "100000" else 	--add 
		"00000001100001" when OpCode = "000100"  else				--beq
		"00001101110000" when OpCode = "001101"  else     			--xori
		"00100000000000" when OpCode = "000010"  else				--jump
		"01000001100001" when OpCode = "000101"  else				--bne
		"00011110100000" when OpCode = "001100"  else 				--lui 
		"10101000000000" when OpCode = "000011" else				--jal
		"XXXXXXXXXXXXXX";      							--undefined	
	jal <= c(13);
	BNE <= c(12);
	Jump <= c(11);
	RegDst <= c(10);    
	RegWrite <= c(9); 
	ALUSrc <= c(8); 
	MemWrite <= c(3);   
	MemRead <= c(2);    
	MemToReg <= c(1); 
	Branch <= c(0);

	
end ContUnitbhv;
