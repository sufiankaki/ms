
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DataMem is
	port(	clk, DMRead, DMWrite, DCHit : in STD_LOGIC; 		-- clock and control line
       		DAddr  : in STD_LOGIC_VECTOR  (31 downto 0);		-- memory address
 		WriteData  : in STD_LOGIC_VECTOR  (127 downto 0);	-- data input
       		ReadData : out STD_LOGIC_VECTOR (127 downto 0));	-- data output

end DataMem;

architecture DataMembhv of DataMem is 
	type Memory is array (0 to 2047) of std_logic_vector (7 downto 0);
	signal temp : Memory := (x"12",x"67",x"34",x"89",
				x"21",x"43",x"57",x"88",
				x"11",x"68",x"A0",x"2F",
				others => (others=> '0'));
	signal temp_in, temp_out: STD_LOGIC_VECTOR(127 downto 0);

	

begin 

	temp_in <= WriteData;
	process(clk)
	begin 
	for i in 0 to 15 loop
		temp_out((127-8*i) downto (120-8*i)) <= temp(to_integer(unsigned(DAddr(7 downto 0))+i));
	end loop;
	
 		if rising_edge(clk) then
		if DMWrite ='1'  then 
			for n in 0 to 15 loop
				temp(to_integer(unsigned(DAddr(11 downto 0))+n)) <= temp_in((127-8*n) downto (120-8*n));
			end loop;
		end if;
		if DMRead = '1' then
			ReadData <= temp_out;
		end if;
		end if;
	end process;
end DataMembhv ;